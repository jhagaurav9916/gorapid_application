import 'package:flutter/material.dart';
import 'package:gorapid_application/main.dart';
import 'package:form_field_validator/form_field_validator.dart';

Widget buildWidget(String fiedName) {
  if (fiedName == "POSTGRADUATION" || fiedName == "GRADUATION") {
    return Container(
        child: Column(
            // making everything intact in the center
            mainAxisAlignment: MainAxisAlignment.center,
            // getting all the widget on top of each other,by using column widget
            children: <Widget>[
          Text(
            fiedName + ' DETAILS',
            style: TextStyle(
              color: Colors.black,
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Degree',
                  hintText: 'Please enter your Degree',
                ),
              )),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Stream',
                  hintText: 'Please enter your Stream',
                ),
              )),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Passing Year',
                ),
              )),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Percentage/CGPA',
                  hintText: 'Please multiply by 10 in case of cgpa(7.5*10=75)',
                ),
              )),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'College/University',
                  hintText: 'Please enter your College/University',
                ),
              )),
        ]));
  } else {
    return Container(
        child: Column(
            // making everything intact in the center
            mainAxisAlignment: MainAxisAlignment.center,
            // getting all the widget on top of each other,by using column widget
            children: <Widget>[
          Text(
            fiedName + ' DETAILS',
            style: TextStyle(
              color: Colors.black,
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Stream',
                  hintText: 'Please enter your Stream',
                ),
              )),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Passing Year',
                  hintText: 'Please enter your Passing Year',
                ),
              )),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Percentage/CGPA',
                  hintText: 'Please multiply by 10 in case of cgpa(7.5*10=75)',
                ),
              )),
          Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'School',
                  hintText: 'Please enter your School',
                ),
              )),
        ]));
  }
}
