import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:gorapid_application/screens/forms/forms_screen.dart';
import 'package:gorapid_application/screens/forms/work_expirence.dart';
// import 'package:gorapid_application/screens/skills_screen.dart';
import 'package:gorapid_application/screens/forms/educations_screen.dart';
import 'package:gorapid_application/screens/forms/skills.dart';
import 'package:gorapid_application/screens/home/landing.dart';
import 'package:gorapid_application/screens/profile/profile_screen.dart';

import 'package:gorapid_application/services/auth.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

Color color1 = HexColor("#F0F0F0");

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TextStyle textStyle = Theme.of(context).textTheme.title;
    var blue = Colors.blue;
    Color c = const Color.fromRGBO(240, 240, 240, 1.0);
    Color f = const Color.fromRGBO(255, 255, 255, 1.0);

    return MaterialApp(
      title: 'GoRapid Ap[plication',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // brightness: Brightness.dark,
        backgroundColor: Colors.grey[700],
        primaryColor: Colors.pink,

        // primarySwatch: Colors.blue[900],
        // textTheme: const TextTheme(bodyText2: TextStyle(color: Colors.white60)),
        // textTheme: Colors.white,
        // accentColor: Colors.blue[800],
        errorColor: Colors.red,
        hintColor: Colors.blue[900],

        inputDecorationTheme: InputDecorationTheme(
          filled: true,
          fillColor: f,
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.blue[900],
                width: 6.0,
              ),
              borderRadius: BorderRadius.circular(10.0)),
          // errorStyle: TextStyle(color: Colors.red, fontSize: 15.0),

          // border: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(10.0),
          // ),
          labelStyle: TextStyle(fontFamily: "Rock Salt", color: blue[900]),
        ),
      ),
      home: AuthService().handleAuth(),
      routes: {
        '/landing_page': (context) => Home(),
        '/application_form': (context) => FormsScreen(),
        '/work_expirence': (context) => WorkExpirence(),
        '/skills_screen': (context) => InputChipExample(),
        '/education_details': (context) => EducationDetails(),
        '/profile_page': (context) => ProfilePage(),
      },
    );
  }
}
