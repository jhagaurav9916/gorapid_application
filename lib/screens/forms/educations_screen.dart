import 'package:flutter/material.dart';
import 'package:gorapid_application/main.dart';
import 'package:gorapid_application/widgets/widgets.dart';
import 'package:form_field_validator/form_field_validator.dart';

class EducationDetails extends StatefulWidget {
  @override
  _EducationDetailsState createState() => _EducationDetailsState();
}

class _EducationDetailsState extends State<EducationDetails> {
  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.menu),
          title: Text('Educational Background'),
          centerTitle: true,
          // backgroundColor: Colors.pink[900],
        ),
        body: Container(
            margin: EdgeInsets.all(22.0),
            // using form class of flutter
            child: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                buildWidget("POSTGRADUATION"),
                Divider(
                  color: Theme.of(context).primaryColor,
                  height: 30,
                  thickness: 4,
                  indent: 10,
                  endIndent: 10,
                ),
                buildWidget("GRADUATION"),
                Divider(
                  color: Theme.of(context).primaryColor,
                  height: 30,
                  thickness: 4,
                  indent: 10,
                  endIndent: 10,
                ),
                buildWidget("INTERMIDIATE"),
                Divider(
                  color: Theme.of(context).primaryColor,
                  height: 30,
                  thickness: 4,
                  indent: 10,
                  endIndent: 10,
                ),
                buildWidget("MATRICULATION"),
                SizedBox(height: 10.0),
                ElevatedButton(
                  child: Text('SUBMIT >>'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/work_expirence');
                  },
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor,
                      padding:
                          EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                      textStyle:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ),
              ],
            ))));
  }
}
