import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:country_state_city_picker/country_state_city_picker.dart';
import 'package:file_picker/file_picker.dart';
import 'dart:io';
import 'dart:convert';
import 'package:gorapid_application/main.dart';
import 'package:form_field_validator/form_field_validator.dart';

class FormsScreen extends StatefulWidget {
  @override
  _FormsScreenState createState() => _FormsScreenState();
}

class _FormsScreenState extends State<FormsScreen> {
  String name;
  String _email;
  String _phonenumber;
  String _whatsAppNumber;
  String _currentCity;
  var _currencies = [
    'Intern',
    'Software Engineer',
    'Sr. Software Engineer',
  ];
  String _currentSelectedValue;
  final double _minimumPadding = 5.0;
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildChooseResume() {
    return RaisedButton(
      onPressed: () async {
        FilePickerResult result = await FilePicker.platform.pickFiles();

        if (result != null) {
          PlatformFile file = result.files.first;

          print(file.name);
          print(file.bytes);
          print(file.size);
          print(file.extension);
          print(file.path);
        }
      },
      child: Text('Choose Resume'),
    );
  }

  Widget _buildUploadResume() {
    return OutlineButton(
      onPressed: () {},
      child: Text('Upload Resume'),
    );
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return Scaffold(
      // backgroundColor: Colors.black,
      appBar: AppBar(
        leading: Icon(Icons.menu),
        title: Text('GENERAL INFORMATION'),
        centerTitle: true,
        // backgroundColor: Colors.pink[900],
      ),

      body: Container(
        margin: EdgeInsets.all(22.0),
        // using form class of flutter
        child: SingleChildScrollView(
          child: Form(
              key: _formKey,
              child: Column(
                // making everything intact in the center
                mainAxisAlignment: MainAxisAlignment.center,
                // getting all the widget on top of each other,by using column widget
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        keyboardType: TextInputType.text,

                        // controller: principalController,
                        validator:
                            RequiredValidator(errorText: "Name is Required* "),
                        // readOnly: true,
                        decoration: InputDecoration(
                          labelText: 'Name',
                          hintText: 'Please enter your name',
                        ),
                        onSaved: (String newValue) => {name = newValue},
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: _minimumPadding, bottom: _minimumPadding),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: "Email",
                        hintText: 'Please enter your EMAIL ADDRESS',
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: MultiValidator([
                        RequiredValidator(errorText: "Email is Required*"),
                        EmailValidator(errorText: "Enter a VAlid Email Address")
                      ]),
                      // readOnly: true,
                      // it will got called after .save()
                      onSaved: (String value) {
                        _email = value;
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  // commonwidget("phonenumber"),
                  // _buildPhoneNumber(),
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: "Phone Number",
                          hintText: 'Please enter your phone number',
                        ),
                        keyboardType: TextInputType.phone,
                        validator: (String value) {
                          String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
                          RegExp regExp = new RegExp(pattern);
                          if (value.length == 0) {
                            return 'Please enter mobile number';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Please enter valid mobile number';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _phonenumber = value;
                        },
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: "WhatsApp Number",
                          hintText: 'Please enter your WhatsApp number',
                        ),
                        keyboardType: TextInputType.phone,
                        validator: (String value) {
                          String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
                          RegExp regExp = new RegExp(pattern);
                          if (value.length == 0) {
                            return 'Please enter WhatsApp number';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Please enter valid WhatsApp number';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _phonenumber = value;
                        },
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        keyboardType: TextInputType.text,

                        // controller: principalController,
                        validator: RequiredValidator(
                            errorText: "Current City Name is Required* "),
                        // readOnly: true,
                        decoration: InputDecoration(
                          labelText: 'City Name',
                          hintText: 'Please enter your Current city',
                        ),
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  FormField<String>(
                    builder: (FormFieldState<String> state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          hintText: "Select Profile to Apply",
                        ),
                        isEmpty:
                            _currentSelectedValue == "Select profile to apply",
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            value: _currentSelectedValue,
                            style: TextStyle(color: Colors.black),
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                _currentSelectedValue = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _currencies.map((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            hint: Text(
                              "Select profile to apply",
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 10.0,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      _buildChooseResume(),
                      _buildUploadResume(),
                    ],
                  ),
                  // _buildChooseResume(),
                  // _buildUploadResume(),
                  SizedBox(
                    height: 10.0,
                  ),
                  ElevatedButton(
                    child: Text('NEXT >>'),
                    onPressed: () {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }
                      // if the input value is correct it will be saved
                      _formKey.currentState.save();
                      // print(_name);
                      Navigator.pushNamed(context, '/work_expirence');
                    },
                    style: ElevatedButton.styleFrom(
                        primary: Theme.of(context).primaryColor,
                        padding:
                            EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                        textStyle: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
