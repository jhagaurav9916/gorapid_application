import 'package:flutter/material.dart';
import 'package:gorapid_application/main.dart';

import 'package:form_field_validator/form_field_validator.dart';

class WorkExpirence extends StatefulWidget {
  @override
  _WorkExpirenceState createState() => _WorkExpirenceState();
}

class _WorkExpirenceState extends State<WorkExpirence> {
  String _companyName;
  String _location;
  String _role;
  String _jobDescription;
  String _servedYears;
  final double _minimumPadding = 5.0;

  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return Scaffold(
      // backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text('Work Experience '),
        centerTitle: true,
        // backgroundColor: Colors.pink[900],
      ),
      body: Container(
        margin: EdgeInsets.all(22.0),
        // using form class of flutter
        child: SingleChildScrollView(
          child: Form(
              key: _formKey,
              child: Column(
                // making everything intact in the center
                mainAxisAlignment: MainAxisAlignment.center,
                // getting all the widget on top of each other,by using column widget
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        validator: RequiredValidator(
                            errorText: "Company Name is Required* "),
                        decoration: InputDecoration(
                          labelText: "Company Name",
                          hintText: 'Please enter your Company Name',
                        ),
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        validator: RequiredValidator(
                            errorText: "Company Location is Required* "),
                        decoration: InputDecoration(
                          labelText: "Company Location ",
                          hintText: 'Please enter your Company Location ',
                        ),
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        validator: RequiredValidator(
                            errorText: "Previous Role is Required* "),
                        decoration: InputDecoration(
                          labelText: "Previous Role ",
                          hintText: 'Please enter your Previous Role',
                        ),
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: RequiredValidator(
                            errorText: "Expirence(In Years) is Required* "),
                        decoration: InputDecoration(
                          labelText: "Expirence(In Years) ",
                          hintText: 'Expirence(In Years)',
                        ),
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          top: _minimumPadding, bottom: _minimumPadding),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        maxLines: 5,
                        validator: RequiredValidator(
                            errorText: "Job Description is Required* "),
                        decoration: InputDecoration(
                          labelText: "Job Description ",
                          hintText: 'Job Description',
                        ),
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  ElevatedButton(
                    child: Text('NEXT >>'),
                    onPressed: () {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }
                      // if the input value is correct it will be saved
                      _formKey.currentState.save();
                      // print(_name);
                      Navigator.pushNamed(context, '/skills_screen');
                    },
                    style: ElevatedButton.styleFrom(
                        primary: Theme.of(context).primaryColor,
                        padding:
                            EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                        textStyle: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
