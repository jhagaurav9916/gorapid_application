import 'package:flutter/material.dart';

class InputChipExample extends StatefulWidget {
  @override
  _InputChipExampleState createState() => new _InputChipExampleState();
}

class _InputChipExampleState extends State<InputChipExample> {
  TextEditingController _textEditingController = new TextEditingController();
  List<String> _values = new List();
  List<bool> _selected = new List();

  @override
  void dispose() {
    _textEditingController?.dispose();
    super.dispose();
  }

  Widget buildChips() {
    List<Widget> chips = new List();

    for (int i = 0; i < _values.length; i++) {
      InputChip actionChip = InputChip(
        selected: _selected[i],
        label: Text(_values[i]),
        labelStyle: TextStyle(fontFamily: "Rock Salt", color: Colors.blue[900]),
        // avatar: FlutterLogo(),
        elevation: 10,
        padding: EdgeInsets.symmetric(),
        pressElevation: 5,
        shadowColor: Colors.blue[900],
        onPressed: () {
          setState(() {
            _selected[i] = !_selected[i];
          });
        },
        onDeleted: () {
          _values.removeAt(i);
          _selected.removeAt(i);

          setState(() {
            _values = _values;
            _selected = _selected;
          });
        },
      );

      chips.add(actionChip);
    }

    return ListView(
      // This next line does the trick.
      scrollDirection: Axis.horizontal,
      children: chips,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CHOOSE SKILLS'),
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                Container(
                  height: 100,
                  child: buildChips(),
                ),
                SizedBox(
                  height: 30.0,
                ),
                TextFormField(
                  controller: _textEditingController,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () {
                      _values.add(_textEditingController.text);
                      _selected.add(true);
                      _textEditingController.clear();

                      setState(() {
                        _values = _values;
                        _selected = _selected;
                      });
                    },
                    child: Text('Submit'),
                  ),
                ),
                SizedBox(
                  height: 200.0,
                ),
                ElevatedButton(
                  child: Text('NEXT >>'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/education_details');
                  },
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor,
                      padding:
                          EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                      textStyle:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ),
              ],
            )),
      ),
    );
  }
}
