import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return new Scaffold(
        body: new Stack(
      children: <Widget>[
        new Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: NetworkImage(
                  "https://images.unsplash.com/photo-1514612426854-2b5a6d49298f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Njd8fHdoaXRlJTIwY29kaW5nJTIwYmFja2dyb3VuZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SingleChildScrollView(
          child: SafeArea(
            child: new Center(
              child: Padding(
                  padding: EdgeInsets.fromLTRB(5.0, 120.0, 5.0, 5.0),
                  child: Column(
                    children: [
                      Text("Welcome To",
                          style: TextStyle(
                            // fontWeight: FontWeight.w100,
                            fontSize: 45.0,
                          )),
                      Text(
                        "Think GoRapid Technologies",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 45.0,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      SizedBox(
                        height: 150.0,
                      ),
                      OutlineButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/profile_page');
                        },
                        borderSide: BorderSide(
                          width: 2.0,
                          color: Colors.blue[800],
                          style: BorderStyle.solid,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Text(
                          'Visit Profile',
                          style: textStyle,
                        ),
                      ),
                      OutlineButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/application_form');
                        },
                        borderSide: BorderSide(
                          width: 2.0,
                          color: Colors.blue[800],
                          style: BorderStyle.solid,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Text(
                          'Application Form',
                          style: textStyle,
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        )
      ],
    ));
  }
}
